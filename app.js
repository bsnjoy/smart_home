#!/usr/bin/env node
process.env.UV_THREADPOOL_SIZE = 128; // https://stackoverflow.com/questions/35387264/node-js-request-module-getting-etimedout-and-esockettimedout
var http = require('http');
var fs = require('fs');
var url = require('url');
var socketio = require('socket.io');
var pg = require('pg');

const path = require("path");
eval(fs.readFileSync(path.resolve(__dirname, './helper.js')) + '');

log('Application started! Waiting for server to start.');
log('Directory: ' + __dirname);

// let connectionString = "postgres://smarthome:w9w3wJfsHUiby@localhost:5432/smarthome";
// var db = new pg.Client(connectionString);
// PGPASSWORD=w9w3wJfsHUiby psql -U "smarthome" -d "smarthome" -h 127.0.0.1
var db = new pg.Client({
	host: '127.0.0.1',
  	port: 5432,
 	user: 'smarthome',
	password: 'w9w3wJfsHUiby',
	database: 'smarthome',
	statement_timeout: 1000,
	query_timeout: 1000,
});
db.connect();

db.on('error', (err, client) => {
	console.error('Unexpected DB error on idle client', err)
	process.exit(-1)
})

var lamps, lampsClient = [];
var sprinklers;
var sprinklersTimeTable = [];
var polivNowTimetableID = -1;
var heats = [];
var sensors = [];

async function reloadLamps() {
	try {
		var sql, result;

		sql = "SELECT * FROM lamps ORDER BY id";
		result = await db.query(sql);
		lamps = result.rows;


		sql = "SELECT id, url, title, state, clip_path, color FROM sprinklers ORDER BY id";
		result = await db.query(sql);
		sprinklers = result.rows;

		sql = "SELECT id, json FROM timetables ORDER BY id";
		result = await db.query(sql);
		sprinklersTimeTable = JSON.parse(result.rows[0].json);

		sql = "SELECT * FROM heats ORDER BY id";
		result = await db.query(sql);
		heats = result.rows;
		
	} catch(e) {
		log('ERROR!!! caught in reloadLamps. May be problem with postgresql' + e);
		process.exit(-1)
  	}
}


reloadLamps().then((result) => {
	// Делаем штуки, которым нужно подождать нашей функции
	log(lamps);
   }).catch((err) => {
	throw err;
   });

// Loading the index file . html displayed to the client
var server = http.createServer(function (req, res) {
	var ip = req.connection.remoteAddress; // ipv6 ::ffff:192.168.1.252
	var parsedURL = url.parse(req.url, true);
	var path = parsedURL.pathname;
	var query = parsedURL.query;
	log('Ask for ' + path + ' with parameters: ' + JSON.stringify(query) + ' from ip: ' + ip);
	if( path == '/api/garage/switch' ) {
		state = 1 - lamps[1].state;
		console.log('change garage state from http api. changing state of pin 1. New value is:' + state);
		res.end('Garage lamp new state is: ' + state);
		digitalWriteAPI(lamps, 1, state, 'lamps');
	} else if ( path == '/api/poliv/-1' ) {
		log('Stop poliv from /api/poliv/-1');
		res.writeHead(200, {});
		res.end('Stop poliv');
		stopPoliv();
	} else if ( path == '/api/poliv/0' ) {
		log('Starting poliv 0 from /api/poliv/0');
		res.writeHead(200, {});
		res.end('Starting poliv in contour 0');
		polivNowTimetableID = 0; // номер контура из API
		startPoliv(0);
	} else if( ip.indexOf('192.168.1.90') != -1 ) {
		log('Teplica is talking to us');	
		switch( path ) {
			case '/pin/0/0':
				digitalWriteAPI(lamps, 12, 0, 'lamps', true);
				break;
			case '/pin/0/1':
				digitalWriteAPI(lamps, 12, 1, 'lamps', true);
				break;
			case '/pin/1/0':
				digitalWriteAPI(lamps, 13, 0, 'lamps', true);
				break;
			case '/pin/1/1':
				digitalWriteAPI(lamps, 13, 1, 'lamps', true);
				break;
			case '/alive/-1':
				log('RESTARTED');
				break;
			default:
				if( path.indexOf('/alive/') == 0 ) {
					alive = path.split('/')[2];
					log('Teplica is alive for: ' + alive);
				} else {
					log('UNKNOWN PATH from .90 Teplica!!!');
				}
				break;
		}
	} else {
		serveLocalFiles(res, path);
	}
});

// Loading socket.io
var io = socketio(server);

function error2client(error) {
	log(error);
	io.sockets.emit('error', error);
}

function query(sql, values) {
	(async function() {
	try {
		result = await db.query(sql, values);
	} catch (err) {
		console.error(err);
	}
   })();
}

// SelfOn - устройство (выключатель в теплице) сам переключает свет. Нас только уведомляет, соответсвенно отправлять заного запрос не надо
async function digitalWriteAPI( object, id, state, TableName, SelfOn = false ) {
	try {
		if( ! SelfOn ) {
			if(object[id].hasOwnProperty('pin_on')) {
				stateAPIValue = Math.abs(object[id].pin_on - state);
			} else {
				stateAPIValue = state;
			}
			body = await doRequest( object[id].url + stateAPIValue );
		}
		object[id].state = state;
		var data = {};
		data[TableName] = object;
		io.sockets.emit('broadcast', data);
		query("UPDATE " + TableName + " SET state=$1 WHERE id=$2", [state, id])
		return true;
	}
	catch(err) {
		error2client('Ошибка в requests url:' +object[id].url + ' err:' + err);
		return false;
	}
}


var timerPoliv = null;

function startPoliv(contour) {
	log('Starting Poliv polivNowTimetableID:' + polivNowTimetableID + ' contour:' + contour);

	if( polivNowTimetableID < 0 ) {
		log('ERROR! Something strange happend, polivNowTimetableID < 0, can not start poliv.');
		return false;
	}

	if( contour == 0 && timerPoliv ) {
		log('Just startPoliv with  first contouк = 0, but there was active timer. Maybe overlapping poliv schedules');
		clearTimeout(timerPoliv);
		timerPoliv = null;
	}

	for (var i = 0; i < sprinklers.length; i++) {
		if(sprinklers[i].state == 1) {
			changeSprinklerState({id: i, state: 0});
		}
	}

	// If countour time is 0, then skip it.
	while( contour < sprinklers.length && sprinklersTimeTable[polivNowTimetableID].durations[contour] == 0 ) {
		log('Contour:' + contour + ' time is 0, skipping it');
		contour++;
	}

	if (contour < sprinklers.length ) {
		changeSprinklerState({id: contour, state: 1});
		// io.sockets.emit('broadcast', {
		// 	sprinklers: sprinklersClient,
		// });
		polivNextTimeSeconds = sprinklersTimeTable[polivNowTimetableID].durations[contour] * sprinklersTimeTable[polivNowTimetableID].units;
		log('Next poliv in ' + polivNextTimeSeconds + ' sec');
		if(timerPoliv) {
			clearTimeout(timerPoliv);
			timerPoliv = null;
		}
		timerPoliv = setTimeout(startPoliv, polivNextTimeSeconds * 1000, contour + 1);
	} else {
		log('Poliv finished');
		stopPoliv();
	}
}

function stopPoliv() {
	if(timerPoliv) {
		clearTimeout(timerPoliv);
		timerPoliv = null;
	}
	polivNowTimetableID = -1;
	for (var i = 0; i < sprinklers.length; i++) {
		if(sprinklers[i].state == 1) {
			changeSprinklerState({id: i, state: 0});
		}
	}

	io.sockets.emit('broadcast', {
		polivNowTimetableID: polivNowTimetableID
	});

	log('Stoping poliv!');
}

function changeSprinklerState(sprinkler) {
	log('sprinklerStateChange:' + sprinkler.id + ' state:' + sprinkler.state);
	digitalWriteAPI(sprinklers, sprinkler.id, sprinkler.state, 'sprinklers');
}



// When a client connects, we note it in the console
io.sockets.on('connection', function (socket) {
	log('A client is connected!');

	socket.emit('broadcast', {
		heats: heats,
		sensors: sensors,
		lamps: lamps,
		sprinklers: sprinklers,
		sprinklersTimeTable: sprinklersTimeTable,
		polivNowTimetableID: polivNowTimetableID
	});

	socket.on('heatStateChange', function (roomID) {
		log('Room heats id: ' + roomID)
		digitalWriteAPI(heats, roomID, 1 - heats[roomID].state, 'heats');
	});

	socket.on('lampStateChange', function (lamp) {
		log('lampStateChange:' + lamp.id);
		digitalWriteAPI(lamps, lamp.id, lamp.state, 'lamps');
	});

	socket.on('startPoliv', function(timeTableNumber) {
		log("Executed Start Poliv:"+timeTableNumber);
		polivNowTimetableID = timeTableNumber;
		startPoliv(0);
		io.sockets.emit('broadcast', {
			polivNowTimetableID: polivNowTimetableID
		});
	});
	
	socket.on('stopPoliv', function() {
		stopPoliv();
	});	

	socket.on('savePolivTimetable', function(newTimetable) {
		log('savePolivTimetable event');
		sprinklersTimeTable = newTimetable;
		query("UPDATE timetables SET json=$1 WHERE id=0", [JSON.stringify(sprinklersTimeTable)]);
	});

	socket.on('sprinklerStateChange', changeSprinklerState);

	socket.on('heatTermalControl', function (data) {
		roomTC = data;
		log('heatTermalControl:' + data);
		heats[data.id].thermal_control_on = data.thermal_control_on;
		heats[data.id].thermal_control_value = data.thermal_control_value;
		query("UPDATE heats SET thermal_control_on=$1, thermal_control_value=$2 WHERE id=$3", [data.thermal_control_on, data.thermal_control_value, data.id]);
		io.sockets.emit('broadcast', { heats: heats } );
	});

});


function loop() {
}

server.on('error', function (err) {
	log(err);
	log('Some one is already listening on 80 port, find and kill it!');
	log('sudo netstat -nlp | grep 80');
	process.exit(code = 1);
});

server.listen(80);

const HEAT_ON = 0;
const HEAT_OFF = 1;
async function updateSensors() {
	log('Updating sensors');
	try {
		sensors = await doRequest('http://api.witts.ru/getdata.php');
		log(JSON.stringify(sensors));
	} catch(err) {
		log('Error updating sensors getting data from witts.ru: ' + err);
	}

	try {
		io.sockets.emit('broadcast', { sensors: sensors } );
		var changed = false;
		for(id = 0; id < sensors.length; id++) {
			if(heats[id].thermal_control_on){
				if( heats[id].thermal_control_value > sensors[id].temp ) {
					if( heats[id].state == HEAT_OFF ) {
						log('В комнате с id=' + id + ' надо нагреть до: ' + heats[id].thermal_control_value + ' а в комнате температура: ' + sensors[id].temp + '. При этом нагрев был выключен, включаем!');
						digitalWriteAPI(heats, id, HEAT_ON, 'heats');
						changed = true;
					}
				} else {
					if( heats[id].state == HEAT_ON ) {
						log('В комнате с id=' + id + ' надо остудить до: ' + heats[id].thermal_control_value + ' а в комнате температура: ' + sensors[id].temp + '. При этом нагрев был включен, выключаем.');
						digitalWriteAPI(heats, id, HEAT_OFF, 'heats');
						changed = true;
					}
				}
			}
		}
		if(changed) {
			io.sockets.emit('broadcast', { heats: heats } );
		}
	} catch(err) {
		log('Error updating sensors sending to clients: ' + err);
	}
}

updateSensors();
setInterval( updateSensors, 300000); // Раз в 5 минут обновлять состояние сенсоров
// setInterval( updateSensors, 10000); // Раз в 10 секунд обновлять состояние сенсооров - для тестов
doRequest( 'http://192.168.1.18/pin/2/0' ); // На поливе включаем  реле которое отсекает напряжение от всех клапанов.

log('Server listening on port 80');

//// Checking if some-one press real switch (button) in the garage.
//setInterval( loop, 200);
