# Управление теплым полом  #
* Основаная плата - Raspberry Pi 2 Model B (Revision: 000e) ![Pinout](./gpoios.png)
* Подсоединяется к блоку из 8ми твердотельных реле 8 SSR (Solid State Relay) [8 Channel 5V DC Relay Module Solid State low level SSR AVR DSP for Arduino](https://ru.aliexpress.com/item/8-Channel-5V-DC-Relay-Module-Solid-State-High-Level-OMRON-SSR-AVR-DSP-for-Arduino/32325083195.html?spm=a2g0s.9042311.0.0.bsFprA)) - заказывал с aliexpress с EMS доставкой - чтобы быстрее пришло. Реле нормально-закрытые, то есть сигнал не идет, то момента пока на управляющий вход не подать НОЛЬ!! Именнь Ноль надо подать для того чтобы пошел сигнал!
* Для соединения Raspberry Pi и 8 SSR Module удобно использовать шлейф из 10 проводов папа-папа (+5V, GND и 8 управляющих сигналов)
* 8 штук Thermotech электроприод (термопривод) (cервопроивод в народе называют, но скорее всего это не правильно) -  работают от 220/24 VAC по инструкции, я тестировал только на 24 VAC для безопасности и тк там уже было подведено. Мощность 3 Вт, Нормально Закрыт (NC), Усилие штонка 105 H, ход штока 3мм, Габариты 45*45*60 мм, Время открытия/закрытия ~ 3 минуты. Открыт до тех пор, пока подано напряжение, то есть пока подан управляющий сигнал 0 В на CH1-CH8 каналы шилда с реле.

### Схема подключения ###
* GND pi - DC- on shield
* 5V pi  - DC+ on shield
* GPIO4  (physical  7) pi - CH1 on shield (SW1 Лестница)
* GPIO17 (physical 11) pi - CH2 on shield (SW2 Кладовка)
* GPIO18 (physical 12) pi - CH3 on shield (SW3 Коридор)
* GPIO27 (physical 13) pi - CH4 on shield (SW4 Ванная)
* GPIO22 (physical 15) pi - CH5 on shield (SW5 Детская)
* GPIO23 (physical 16) pi - CH6 on shield (SW6 Спальня)
* GPIO24 (physical 18) pi - CH7 on shield (SW7 Гардероб)
* GPIO25 (physical 22) pi - CH8 on shield (SW8 Офис)

На шилде 8 SSR на выходах SW1-SW8 левая клеммы соединяется друг другом посредством пайки и подключается к одному из выходов 24V

### Автозапуск скрипта ###
* sudo vim /etc/rc.local   /etc/init.d/local
* Добавляем строчку
* node /root/smart-home/app.js >> /root/smart-home/error.log 2>&1 &
* update-rc.d local defaults 80

### Автозапуска полива ###
crontab -e  
0 18 * * * wget -qO- http://192.168.1.15:8080/api/poliv/0 &> /dev/null

### Список ПО необходимого для открытия файлов ###

* nodejs - на нем написан сам сервер и управление  
sudo apt update  
curl -sL https://deb.nodesource.com/setup_12.x | bash -  
sudo apt-get install -y nodejs  
* Установка socket.io из package.json:
* npm install
