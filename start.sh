#!/bin/bash
sleep 10
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
/root/.nvm/versions/node/v16.16.0/bin/node /root/smart-home/app.js >> /root/smart-home/error.log 2>&1 &
