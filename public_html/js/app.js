var socket = io.connect(location.href);

Vue.component('tabs', {
	template: `
<div>
	<div class="tabs">
		<ul>
		<li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }">
			<a :href="tab.href" @click="selectTab(tab)">
				<span v-if="tab.icon" class="icon is-small"><i :class="tab.icon"></i></span>
				{{ tab.name }}
			</a>
		</li>
		</ul>
	</div>

	<div class="tabs-details">
		<slot></slot>
	</div>
</div>
`,
	data() {
		return { tabs: [] };
	},
	created() {
		this.tabs = this.$children;
	},
	methods: {
		selectTab(selectedTab) {
			this.tabs.forEach(tab => {
				tab.isActive = (tab.name == selectedTab.name);
			});
		}
	}
});

Vue.component('tab', {
	template: `
<div v-show="isActive"><slot></slot></div>
`,
	props: {
		name: { required: true },
		icon: { default: false },
		selected: { default: false }
	},
	data() {
		return {
			isActive: false
		};
	},
	computed: {
		href() {
			return '#' + this.name.toLowerCase().replace(/ /g, '-');
		}
	},
	mounted() {
		this.isActive = window.location.hash ? this.href == decodeURI(window.location.hash) : this.selected
	}
});

var app = new Vue({
	el: '#app',
	data: {
		lamps: [],
		sprinklers: [],
		sprinklerOnColor: 'blue',
		sprinklersTimeTable: [],
		polivNowTimetableID: -1, // Полив текущий -1 = выключен, или номер расписания
		heats: [],
		floorTitles: ['Подвал', '1 этаж', '2 этаж'],
		sensors: [],
		error: '',
	},
	methods: {
		lampStateChange(id) {
			socket.emit( 'lampStateChange', { id: id, state: 1 - this.lamps[id].state } );
		},
		sprinklerStateChange(id) {
			// Выключаем текущий работающий спринклер
			currentSprinkler = app.sprinklers.find(x => x.state === 1);
			if(typeof currentSprinkler !== 'undefined') {
				if(currentSprinkler.id != id) {
					socket.emit( 'sprinklerStateChange', { id: currentSprinkler.id, state: 0 } );
				}
			}
			socket.emit( 'sprinklerStateChange', { id: id, state: 1 - this.sprinklers[id].state } );
			//this.sprinklers[id].state = 1 - this.sprinklers[id].state;
			//console.log(this.sprinklers[id].state);
		},
		allLampsStateChange(state) {
			for(var i=0; i<this.lamps.length; i++) {
				socket.emit( 'lampStateChange', { id: i, state: state } );
				
			}
		},
		startPoliv(timeTableNumber) {
			console.log(timeTableNumber);
			socket.emit( 'startPoliv', timeTableNumber );
		},
		stopPoliv() {
			socket.emit( 'stopPoliv' );
		},
		removePolivTimetableColumn(columnNumber) {
			this.$delete(this.sprinklersTimeTable, columnNumber);
		},
		addPolivTimetableColumn() {
			this.sprinklersTimeTable.push({title: 'Новое расписание', timeStart: '10:10', units: 60, durations: Array.from({ length: this.sprinklers.length }, (v, k) => 0) });
		},
		savePolivTimetable() {
			socket.emit( 'savePolivTimetable', this.sprinklersTimeTable );
		},
		heatStateChange(roomId, event) {
			if (event.target.localName != "input") {
				//Vue.set(app.roomStatus, roomId, 1 - app.roomStatus[roomId]);
				socket.emit('heatStateChange', roomId);
			}
		},
		heatTermalControlStateChange(roomID) {
			//Vue.set(app.roomTC[roomId], 0, 1 - app.roomTC[roomId][0]);
			socket.emit('heatTermalControl', { id: roomID, thermal_control_on: 1-this.heats[roomID].thermal_control_on, thermal_control_value: this.heats[roomID].thermal_control_value } );
		},
		heatTermalControlValueChange(roomID) {
			socket.emit('heatTermalControl', { id: roomID, thermal_control_on: this.heats[roomID].thermal_control_on, thermal_control_value: this.heats[roomID].thermal_control_value } );
			//socket.emit('roomTC', app.roomTC);
		},
		move(event) {
			if (typeof part === "undefined") return;
			x = event.clientX - document.getElementById('app').getBoundingClientRect().x;
			y = event.clientY - document.getElementById('app').getBoundingClientRect().y;
			switch (part) {
				case 'top':
					Vue.set(app.rooms[id].main, 0, x);
					Vue.set(app.rooms[id].main, 1, y);
					break;
				case 'size':
					Vue.set(app.rooms[id].main, 2, x - app.rooms[id].main[0]);
					Vue.set(app.rooms[id].main, 3, y - app.rooms[id].main[1]);
					break;
			}
			app.rooms[id].title = app.rooms[id].main[0] + ', ' + app.rooms[id].main[1] + '  ' + app.rooms[id].main[2] + '*' + app.rooms[id].main[3];
			console.log(event)
			console.log(event.offsetX);
		}
	},
	watch: {
		roomTC: function (val) {
			//      console.log(app.roomTC);
			//      socket.emit('roomTC', app.roomTC);
		}
	}
});

//$.get('http://api.witts.ru/getdata.php', { type: 'floor2' }, function (data) { app.sensors = JSON.parse(data); });
//console.log('hi');
//setTimeout( live, 500);
function live() {
	console.log('reload');
	location.reload();
}

// receive current status of rooms from server
socket.on('broadcast', function (data) {
	if (typeof data.heats !== "undefined") { app.heats = data.heats; }
	if (typeof data.sensors !== "undefined") { app.sensors = data.sensors; }
	if (typeof data.lamps !== "undefined") { app.lamps = data.lamps; }
	if (typeof data.sprinklers !== "undefined") { app.sprinklers = data.sprinklers; }
	if (typeof data.sprinklersTimeTable !== "undefined") { app.sprinklersTimeTable = data.sprinklersTimeTable; }
	if (typeof data.polivNowTimetableID !== "undefined") { app.polivNowTimetableID = data.polivNowTimetableID; }
	console.log(data);
});
// receive error from server
socket.on('error', function (error) {
	app.error = error;
});
