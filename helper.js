const request = require('request');
eval(fs.readFileSync(path.resolve(__dirname, './config.js')) + '');
// Convenient date format for logging
function getDateTime() {
  var date = new Date();

  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;

  var min  = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec  = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day  = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return year + "." + month + "." + day + " " + hour + ":" + min + ":" + sec;

}

function log( txt ) {
  var time = getDateTime();
  console.log(time + ' ' + txt);
}


// https://stackoverflow.com/questions/38428027/why-await-is-not-working-for-node-request-module
// I found if there are too many async requests, then ESOCKETTIMEDOUT exception happens in linux. The workaround I've found is doing this: pool: {maxSockets: 100}
// https://github.com/request/request/issues/2738
function doRequest(url, params = { timeout: 10000, json: true, agent: false, pool: {maxSockets: 100} }) {
	return new Promise(function (resolve, reject) {
		request(url, params, function (error, res, body) {
			if (!error && res.statusCode == 200) {
				resolve(body);
			} else {
				reject(error);
			}
		});
	});
}

function riot( message ) {
	url = 'https://matrix.ilfumo.ru/_matrix/client/r0/rooms/' + config.room + '/send/m.room.message?access_token=' + config.bot_matrix_token;
	payload = {'msgtype':'m.text','body':message};
	request.post( url ).form( payload );
}

function digitalRead(pin)
{
    var output = fs.readFileSync('/sys/class/gpio/gpio' + pin + '/value', "utf8");
    var values = output.split(":");
    var result = Number(values[values.length - 1]);

    //console.log(result);
    return result
}

function digitalWrite(pin, value) {
  fs.writeFileSync('/sys/class/gpio/gpio' + pin + '/value', String(value));
}

function pinMode(pin, mode) {
  try {
  // it's ok to receive error here - it means we already started script before and file exists
    fs.writeFileSync('/sys/class/gpio/export', String(pin));
  } catch(err) {
    log(err);
  }
  try {
  // error here usually happens when run not under root
  // TODO learn and setup wiringPi to be able to run as pi user
    fs.writeFileSync('/sys/class/gpio/gpio' + pin + '/direction', String(mode)); // mode = out, in
  } catch(err) {
    log(err);
    log('OMG use sudo !!!!!!!!!!');
    log('sudo node app.js');
    process.exit(code=1)
  }
}

function delay(ms) {
  var start = new Date().getTime();
  while(1) {
    if ((new Date().getTime() - start) > ms) {
      break;
    }
  }
}

// Send file content to client
function sendFile( res, file) {
//  fs.readFile( __dirname + file, 'utf-8', function(err, content) {
  fs.readFile( __dirname + file, function(err, content) {
    if (!err) {
      res.end( content );
    } else {
      log( 'Error, reading file: ' + err );
      res.end( 'Error, reading file ' + file + '. Check log' );
    }
  });
}

var htmlPath = "/public_html";

function serveLocalFiles(res, path) {
	switch (path.split('.').pop()) {
		case '/':
		case 'html':
			res.writeHead(200, { 'Content-Type': 'text/html' });
			sendFile(res, htmlPath + '/index.html');
			break;
		case 'css':
			res.writeHead(200, { 'Content-Type': 'text/css' });
			sendFile(res, htmlPath + path);
			break;
		case 'js':
			res.writeHead(200, { 'Content-Type': 'text/javascript' });
			sendFile(res, htmlPath + path);
			break;
		case 'ico':
			res.writeHead(200, { 'Content-Type': 'image/x-icon' });
			sendFile(res, htmlPath + path);
			break;
		case 'woff2':
			res.writeHead(200, { 'Content-Type': 'font/opentype' });
			sendFile(res, htmlPath + path);
			break;
		case 'jpg':
			res.writeHead(200, { 'Content-Type': 'image/jpeg' });
			sendFile(res, htmlPath + path);
			break;
		case 'map':
			res.writeHead(200, { 'Content-Type': 'application/json' });
			sendFile(res, htmlPath + path);
			break;
		default:
			res.writeHead(404, { 'Content-Type': 'text/plain' });
			res.write('404 Not found');
			log('No case for:' + path);
			break;
	}
}
